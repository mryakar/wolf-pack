//// DRIVER FOR THE ARDUINO ROBOT #2 ////

// SLAVE RECEIVER - I2C

/*

This program works for the procesing the data and terminating it for motion. It takes the data from the I2C bus. The data has a special form. For example;

"1_L230R122" or "2_L23R145" or "Swarm_L34R112".

- The piece of line which comes before the underscore character detemines which Arduino Robot or Robots will be moving.
If it is "1", the first robot will be moving. If it is "2", the second one moving. On the other hand, if it is "Swarm", Arduino Robots will be moving together like a swarm.

- The piece of line which comes after the character "L" and before "R" determine the left motor's speed.

- The piece of line which comes after the character "R" determine the right motor's speed.

NOTE: This program must be uploaded to the Arduino Robot's Control Board. <ArduinoRobot.h> library has <Wire.h> library's information, so to avoid any confliction between 
<ArduinoRobot.h> and <Wire.h> libraries, before you upload this program to the board, delete the Wire library or change its name on the directory "\Program Files (x86)\Arduino\libraries" 
or "\Program Files\Arduino\libraries".

Created on 19 June 2014
by Ahmet Yakar

*/

#include <ArduinoRobot.h> // This library is imported for Arduino Robot's properties.
#include <Wire.h> // This library is imported for I2C.

String firstData = ""; //An empty string declaration for the data received at the very first place.
int compassValue; // An integer declaration for the compass value.


void setup()
{
  Robot.begin(); // Here Arduino Robot Begins.
  Wire.begin(10); // I2C bus begins with the device address of #10
  Wire.onReceive(receiveEvent); // The receiving process is registered as an event.
  Serial.begin(9600); // Serial bus begins with 9600 baud rate.
  Robot.beginTFT(); // Arduino Robot's TFT screen is started.
}

void loop()
{ 
  compassValue = Robot.compassRead(); // The compass values is read and initialized to the corresponding integer.
  Robot.drawCompass(compassValue); // The compass value is drawn to the screen.
  delay(100); // Processing is paused fo 100 milliseconds.
}

void receiveEvent(int howMany) // The event for receiving process
{
  while(Wire.available() > 1) // This line works when there is an incoming transmission which has more than one characters from the I2C bus. However, it does not receive the last character.
  {
    char c = Wire.read(); // This line receives the byte as a character.
    Serial.print(c); // This line prints the character on the SERIAL monitor for debugging.
    firstData+=c; // Received character is added to the firstData string.

  }
  char x = Wire.read(); // This line also receives the byte as a character.
  Serial.println(x); // This line prints the character on the SERIAL monitor for debugging.
  firstData+=x; // Received character is added to the firstData string.
  timeToRun(firstData); // The received line is sent to the motion function.
}
void timeToRun(String command) { // The motion function for the Arduino Robot
  
  if(command.equals("MODECHANGE") || command.equals("PLAYERCHANGE")) { // This line works when the command is "MODECHANGE" or "PLAYERCHANGE".
    Robot.motorsStop(); // Motors stop for changing process.
    delay(3000); //The general processing is paused for 3000 milliseconds.
  }
  else { // This line works when the command is not "MODECHANGE" or "PLAYERCHANGE".
    
    int indexOf_ = command.indexOf('_'); // The index of the underscore character.
    int indexOfL = command.indexOf('L'); // The index of the character "L".
    int indexOfR = command.indexOf('R'); // The index of the character "R".

    String robotID = command.substring(0,indexOf_); // Substring for the Arduino Robot's additional ID.
    String unParsedLeft = command.substring(indexOfL+1,indexOfR); // The unparsed substring for the left motor's speed.
    String unParsedRight = command.substring(indexOfR+1); // The unparsed substring for the right motor's speed.

    int parsedLeft = unParsedLeft.toInt(); // The left motor's speed which is parsed to the integer.
    int parsedRight = unParsedRight.toInt(); // The right motor's speed which is parsed to the integer.

    Serial.print(parsedLeft); // This line prints the left motor's speed on the SERIAL monitor for debugging.
    Serial.println(parsedRight); // This line prints the right motor's speed on the SERIAL monitor for debugging.
    
    if(robotID.equals("2") || robotID.equals("Swarm")) { // This line works when the Arduino Robot's ID is "1" or "Swarm". On the other hand, this line determines the additional ID for this Arduino Robot.
     
      if(parsedLeft==0 && parsedRight==0) { // This line works when the motor speeds are zero.
        Robot.motorsStop(); // Motors stop.
      }
      else { // This line works when the motor speeds are different than zero.
        Robot.motorsWrite(parsedRight, parsedLeft); // Motors run with the speed data of parsedRight and parsedLeft.
      }
    }    
  }
  firstData = ""; // The firstData string is unloaded for the next command.
}
