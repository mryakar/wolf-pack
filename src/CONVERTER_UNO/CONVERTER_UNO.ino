//// CONVERTER FROM SERIAL TO I2C ////

// MASTER WRITER - I2C

/*

This program works for converting data from SERIAL to I2C. It takes the data from SERIAL bus between " ! " characters. The first " ! " character tells the program that there is an
incoming data from the SERIAL bus and, the second " ! " character tells the program that data transfering from the SERIAL bus has ended. For example;

"!HelloWorld!"
"!ExplorationIsTheCuriosity!"
"!123!"
"!X!"

That is why the data which will be sent to I2C device have to be always between " ! " characters. Otherwise, the program cannot estimate where to start or stop.

Created on 19 June 2014
by Ahmet Yakar

*/

#include <Wire.h> // This library is imported for I2C.

String command = ""; // Empty command string declaration.
int count = 1; // An integer declaration which counts the number of " ! " character.

void setup() {
  
  Serial.begin(9600); // Serial bus begins with 9600 baud rate.
  Wire.begin(); //  I2C bus begins.
  
}

void loop() {
  
  if(Serial.available()) { // This line works when there is an incoming data transfering . 
    char x = Serial.read(); // This line reads every character's ASCII value and saves it into memory as it's character form . 
    
    if(x=='!') { // When the incoming character is " ! ", this line works.
      count+=1; // This increase the count character 
      
      ///////////////////////////////////////////// 
      if(command.length()!=0){ // This line works when there is at least one character in the command string closed by " ! " characters. For example "!HelloWorld!" or "!X!.
        Serial.println(command); // This line is for check on the serial monitor whether the command is completely taken or not.
        Wire.beginTransmission(10); // Here the transmission starts to the address device #10.
        
        for(int x=0; x<command.length(); x+=1) {///
          int current = command.charAt(x);///////// On these lines, the command is transfered character by character with their ASCII values.
          Wire.write(current);/////////////////////
        }
        
        Wire.endTransmission(); // Here the transmission end with the address device #10
        command = ""; // The command string is unloaded for the next command.
        ///////////////////////////////////////////
        
      }
    }
    else if(count%2==0) { // This line works when count integer is even.
      command+=x; // When the incoming character is different than " ! ", it is added to the command string.
    }
  }
}
