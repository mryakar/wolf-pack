//// THE MAIN CONTROLLER ////

/*

 This program gets some digital and analog input values from the Arduino Esplora, 
 and sends them to Arduino Robot with a transceiver. These input values are used for 
 Arduino Robot's motion. Arduino Robot can be moved in three different ways. To move 
 the Arduino Robot, this program has 3 different modes;
 
 The first one is "Button Mode", and it uses the "buttons()" function written in the 
 program. It takes digital input values from SWITCH buttons on the Arduino Esplora, 
 and writes them to the SERIAL bus. When this mode is active, the RGB LEDwill be blue.
 
 The second one is "Accelerometer Mode", and it uses the "accelerometer()" function
 written in the program. It takes analog input values from the accelerometer on the
 Arduino Esplora, and writes them to the SERIAL bus. When this mode is active,the RGB
 LED will be green.
 
 The last one is "Joystick Mode", and it uses the "joystick()" function written in the
 program. It takes analog input values from the joystick on the Arduino Esplora, and 
 writes them to the SERIAL bus. When this mode is active,the RGB LED will be yellow.
 
 To change the motion mode, press and hold the SWITCH 3 and JOYSTICK BUTTON for approximately
 2 seconds. In this 2 seconds, the RGB led will blink for 3 times in red colour and buzzer
 will make high frequency beep sounds with RGB led at the same time. After 2 seconds, the RGB led
 will be red, and the buzzer will make a longer beep. This shows that
 the mode is changed. If you release the buttons before 2 seconds, the current mode
 will not change.
 
 Also this program can switch between Arduino Robots or control them at the same time. To switch 
 the robot, press and hold the SWITCH 1 and JOYSTICK BUTTON for approximately
 2 seconds. In this 2 seconds, the RGB led will blink for 3 times in red colour and buzzer
 will make low frequency beep sounds with RGB led at the same time. After 2 seconds, the RGB led
 will be red, and the buzzer will make a longer beep. This shows that
 the robot is switched. If you release the buttons before 2 seconds, the current robot
 will not change.
 
 This program sends data in a special format. For example;
 
 "1_L230R122" or "2_L23R145" or "Swarm_L34R112".
 
 - The piece of line which comes before the underscore character detemines which Arduino Robot or Robots will be moving.
If it is "1", the first robot will be moving. If it is "2", the second one moving. On the other hand, if it is "Swarm", Arduino Robots will be moving together like a swarm.

- The piece of line which comes after the character "L" and before "R" determine the left motor's speed.

- The piece of line which comes after the character "R" determine the right motor's speed.
 
 
 Created on 18 June 2014
 by Ahmet Yakar
 
 */

#include <Esplora.h> // This library is imported for Arduino Esplora's properties.

int modeInteger = 3; // Integer for motion mode changing.
int playerInteger = 3; // Integer for probot switching.

void setup()
{
  Serial1.begin(9600); // Serial bus begins with 9600 baud rate.
  delay(2000); // Processing stops for 2000 milliseconds for starting animation.
  Serial1.write("!START!"); // Starting command is sent.(Actually, this is for debugging. It has no effect.)
  start(); // Starting animations are executed.
} 

void loop() {
/*
  - playerInteger determines which robot will be controlled. playerInteger's default value is 3.
  - modeInteger determines which motion mode will be on. modeInteger's default value is 3.
*/
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  // ARDUINO ROBOT 1
  if(playerInteger%3==0) { // This line works when the remaining is zero for dividing playerInteger by 3. 
    Serial1.print("!1_"); // First Robot's additional ID is written to the SERIAL bus with the start transmission character(" ! ").
    if(modeInteger%3==0) { // This line works when the remaining is zero for dividing modeInteger by 3.
      buttons(); // buttons() function is executed.
    }
    else if(modeInteger%3==1){ // This line works when the remaining is one for dividing modeInteger by 3.
      accelerometer(); // accelerometer() function is executed.
    }
    else { // This line works when the remaining is two for dividing modeInteger by 3.
      joystick(); // joystick() function is executed.
    }
  }
  //  ARDUINO ROBOT 1
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  // ARDUINO ROBOT 2
  else if(playerInteger%3==1) { // This line works when the remaining is one for dividing playerInteger by 3.
    Serial1.print("!2_");
    if(modeInteger%3==0) {
      buttons();
    }
    else if(modeInteger%3==1){
      accelerometer();
    }
    else {
      joystick();
    }
  }
  // ARDUINO ROBOT 2
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  // BOTH ARDUINO ROBOTS WILL MOVE TOGETHER
  else { // This line works when the remaining is two for dividing playerInteger by 3.
    Serial1.print("!Swarm_"); // "Swarm" is written to the SERIAL bus with the start transmission character(" ! ") to make Robots move together.
    if(modeInteger%3==0) { 
      buttons(); // buttons() function is executed.
    }
    else if(modeInteger%3==1){
      accelerometer();
    }
    else {
      joystick();
    }
  }
  // BOTH ARDUINO ROBOTS WILL MOVE TOGETHER
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  delay(100); // Proccesing stops for 100 milliseconds for stability.
}

void buttons() { // buttons() function.
  
  ////////////////////////////////////// Buttons on Esplora are declared for this function.////////////////////////////////////// 
  int switch1 = Esplora.readButton(SWITCH_DOWN);
  int switch2 = Esplora.readButton(SWITCH_LEFT);
  int switch3 = Esplora.readButton(SWITCH_UP);
  int switch4 = Esplora.readButton(SWITCH_RIGHT);
  int joystickVal = Esplora.readJoystickSwitch();
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////// Motion data are sent according to the buttons' states.////////////////////////////////////
  if(switch1==1 && switch2==1 && switch3==0 && switch4==1) {
    Serial1.print('L');
    Serial1.print(255);
    Serial1.print('R');
    Serial1.print(255);
    Serial1.println('!');
  }
  else if(switch1==0 && switch2==1 && switch3==1 && switch4==1) {
    Serial1.print("L");
    Serial1.print(-255);
    Serial1.print("R");
    Serial1.print(-255);
    Serial1.println("!");
  }
  else if(switch1==1 && switch2==0 && switch3==0 && switch4==1) {
    Serial1.print("L");
    Serial1.print(155);
    Serial1.print("R");
    Serial1.print(255);
    Serial1.println("!");
  }
  else if(switch1==1 && switch2==1 && switch3==0 && switch4==0) {
    Serial1.print("L");
    Serial1.print(255);
    Serial1.print("R");
    Serial1.print(155);
    Serial1.println("!");
  }
  else if(switch1==1 && switch2==0 && switch3==1 && switch4==1) {
    Serial1.print("L");
    Serial1.print(-255);
    Serial1.print("R");
    Serial1.print(255);
    Serial1.println("!");
  }
  else if(switch1==1 && switch2==1 && switch3==1 && switch4==0) {
    Serial1.print("L");
    Serial1.print(255);
    Serial1.print("R");
    Serial1.print(-255);
    Serial1.println("!");
  }
  else if(switch1==0 && switch2==0 && switch3==1 && switch4==1) {
    Serial1.print("L");
    Serial1.print(-155);
    Serial1.print("R");
    Serial1.print(-255);
    Serial1.println("!");
  }
  else if(switch1==0 && switch2==1 && switch3==1 && switch4==0) {
    Serial1.print("L");
    Serial1.print(-255);
    Serial1.print("R");
    Serial1.print(-155);
    Serial1.println("!");
  }
  else {
    Serial1.print("L");
    Serial1.print(0);
    Serial1.print("R");
    Serial1.print(0);
    Serial1.println("!");
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  Esplora.writeRGB(0, 0, 255); // RGB led will be blue for this motion mode.
 

 //// Explanations which are about motion mode chaning and robot switching on the below are valid for the other same code fragments in the program. ////
 
  ////////////////////////////////////////////Code fragment for changing the motion mode.////////////////////////////////////////
  if(switch3==0 && joystickVal==0) { // This works with pressing switch3 and joystick buttons at the same time
    int count = 0; // Counting integer.
    Serial1.println("!MODECHANGE!"); // Mode change notification is sent.(Actually, this is for debugging. It has no effect.)
    while(switch3==0 && joystickVal==0) { // Loop starts with pressing switch3 and joystick buttons at the same time. 
      
      /// Buttons' states are updated at every single loop. If user stops holding the buttons, changing process will be canceled.///
      switch3 = Esplora.readButton(SWITCH_UP);
      joystickVal = Esplora.readJoystickSwitch();
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     
      count+=1; //Here it counts.

      Esplora.writeRGB(0, 0, 0); // RGB led will be closed here.
      
      if(count%200==0) { // This line works every 200 milliseconds to show that changing process is on.
        Esplora.tone(1200, 50); // Buzzer sounds a high frequency beep.
        Esplora.writeRGB(255,0,0); // RGB will create a red blink.
      }

      if(count==800) { //When count is 800 milliseconds changing process is completed.
        Esplora.tone(1500, 500); // Buzzer sounds a high frequency long beep.
        modeInteger+=1; // Motion mode is changed by increasing the modeInteger by one.
        Serial1.println("Accelerometer mode is on."); // Mode change notification is sent.(Actually, this is for debugging. It has no effect.)
        delay(500); // Processing stops for 500 milliseconds for the stability.
        break; // Loop is closed here to avoid an additional changing process in case of the buttons are held continuously even after changing process. 
      }
      delay(1); // Process is paused for 1 millisecond for stability.
    }
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////Code fragment for switching the robot./////////////////////////////////////////////
  if(switch1==0 && joystickVal==0) {
    int count = 0; // Counting integer.
    Serial1.println("!PLAYERCHANGE!"); // Robot switch notification is sent.(Actually, this is for debugging. It has no effect.)
    while(switch1==0 && joystickVal==0) { // Loop starts with pressing switch1 and joystick buttons at the same time. 
      
      /// Buttons' states are updated at every single loop. If user stops holding the buttons, changing process will be canceled.///
      switch1 = Esplora.readButton(SWITCH_DOWN);
      joystickVal = Esplora.readJoystickSwitch();
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      
      count+=1; //Here it counts

      Esplora.writeRGB(0, 0, 0); // RGB led will be closed here.

      if(count%200==0) { // This line works every 200 milliseconds to show that switching process is on.
        Esplora.tone(700, 50); // Buzzer sounds a high frequency beep.
        Esplora.writeRGB(255,0,0); // RGB will create a red blink.
      }

      if(count==800) { //When count is 800 milliseconds switching process is completed.
        Esplora.tone(1500, 500);  // Buzzer sounds a high frequency long beep.
        playerInteger+=1; // Robot is switched by increasing the playerInteger by one.
        delay(500); // Processing stops for 500 milliseconds for the stability.
        break; // Loop is closed here to avoid an additional switching process in case of the buttons are held continuously even after switching process.
      }
      delay(1); // Processing stops for 1 millisecond for the stability.
    }
  }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}

/*
####################################################################################################################################
# Motion data which are sent according to the accelerometer's and joystick's states are calculated in a complex way. Analytical plane
is used for this process. Check the graph to see the areas and motion direction according to the corresponding area.
####################################################################################################################################
*/

void accelerometer() { // accelerometer() function.
  
  ////////////// Values comes from integrated accelerometer on Esplora and buttons are declared for this function.///////////////
  int xAxis = Esplora.readAccelerometer(X_AXIS);
  int yAxis = Esplora.readAccelerometer(Y_AXIS);
  int switch3 = Esplora.readButton(SWITCH_UP);
  int switch1 = Esplora.readButton(SWITCH_DOWN);
  int joystickVal = Esplora.readJoystickSwitch();
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
  ////////////////////////////// Values are calibrated from interval [-180,180] to [255,-255].////////////////////////////////////
  xAxis = map(xAxis,-180,180,255,-255);
  yAxis = map(yAxis,-180,180,255,-255);
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  if(yAxis>0 && xAxis==0 && yAxis>xAxis) { // AREA 1
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis>0 && yAxis>xAxis) { // AREA 2
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis-xAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis>0 && yAxis==xAxis) { // AREA 3
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis>0 && yAxis<xAxis) { // AREA 4
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis-xAxis);
    Serial1.println("!");
  }
  else if(yAxis==0 && xAxis>0 && yAxis<xAxis) { // AREA 5
    Serial1.print("L");
    Serial1.print(xAxis);
    Serial1.print("R");
    Serial1.print(-xAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis>0 && abs(yAxis)<xAxis) { // AREA 6
    Serial1.print("L");
    Serial1.print(yAxis-xAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis>0 && abs(yAxis)==xAxis) { // AREA 7
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  } 
  else if(yAxis<0 && xAxis>0 && abs(yAxis)>xAxis) { // AREA 8
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis+xAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis==0 && abs(yAxis)>xAxis) { // AREA 9
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis<0 && yAxis<xAxis) { // AREA 10
    Serial1.print("L");
    Serial1.print(yAxis-xAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis<0 && yAxis==xAxis) { // AREA 11
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis<0 && yAxis>xAxis) { // AREA 12
    Serial1.print("L");
    Serial1.print(xAxis-yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis==0 && xAxis<0 && yAxis>xAxis) { // AREA 13
    Serial1.print("L");
    Serial1.print(xAxis);
    Serial1.print("R");
    Serial1.print(-xAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis<0 && yAxis<abs(xAxis)) { // AREA 14
    Serial1.print("L");
    Serial1.print(yAxis+xAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis<0 && yAxis==abs(xAxis)) { // AREA 15 
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis<0 && yAxis>abs(xAxis)) { // AREA 16
    Serial1.print("L");
    Serial1.print(yAxis+xAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis==0 && xAxis==0) { // AREA 17
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  Esplora.writeRGB(0, 125, 0); // RGB led will be green for this motion mode.

  if(switch3==0 && joystickVal==0) {

    int count = 0; 
    Serial1.println("!MODECHANGE!");
    while(switch3==0 && joystickVal==0) {
      switch3 = Esplora.readButton(SWITCH_UP);
      joystickVal = Esplora.readJoystickSwitch();
      count+=1; 

      Esplora.writeRGB(0, 0, 0);

      if(count%200==0) {
        Esplora.tone(1200, 50);
        Esplora.writeRGB(255,0,0);
      }
      if(count==800) { 
        Esplora.tone(1500, 500); 
        modeInteger+=1;
       Serial1.println("Joystick mode is on.");
        delay(500);
        break;
      }
      delay(1);
    }
  }
 
 if(switch1==0 && joystickVal==0) {
    int count = 0;
    Serial1.println("!PLAYERCHANGE!");
    while(switch1==0 && joystickVal==0) {
      switch1 = Esplora.readButton(SWITCH_DOWN);
      joystickVal = Esplora.readJoystickSwitch();
      count+=1; 

      Esplora.writeRGB(0, 0, 0);

      if(count%200==0) {
        Esplora.tone(700, 50);
        Esplora.writeRGB(255,0,0);
      }

      if(count==800) { 
        Esplora.tone(1500, 500); 
        playerInteger+=1;
        delay(500);
        break;
      }
      delay(1);
    }
  }
  
}

void joystick() { // joystick() function.
  
  //////////****//////// Values comes from joystick on Esplora and buttons are declared for this function.////////////////////////
  int xAxis = Esplora.readJoystickX();        // read the joystick's X position
  int yAxis = Esplora.readJoystickY();        // read the joystick's Y position
  int switch3 = Esplora.readButton(SWITCH_UP);
  int switch1 = Esplora.readButton(SWITCH_DOWN);
  int joystickVal = Esplora.readJoystickSwitch();
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  ////////////////////////////// Values are calibrated from interval [-512,512] to [255,-255].////////////////////////////////////
  xAxis = map(xAxis,-512,512,255,-255);
  yAxis = map(yAxis,-512,512,255,-255);
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  if(yAxis>0 && xAxis==0 && yAxis>xAxis) { // AREA 1
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis>0 && yAxis>xAxis) { // AREA 2
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis-xAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis>0 && yAxis==xAxis) { // AREA 3
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis>0 && yAxis<xAxis) { // AREA 4
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis-xAxis);
    Serial1.println("!");
  }
  else if(yAxis==0 && xAxis>0 && yAxis<xAxis) { // AREA 5
    Serial1.print("L");
    Serial1.print(xAxis);
    Serial1.print("R");
    Serial1.print(-xAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis>0 && abs(yAxis)<xAxis) { // AREA 6
    Serial1.print("L");
    Serial1.print(yAxis-xAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis>0 && abs(yAxis)==xAxis) { // AREA 7
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  } 
  else if(yAxis<0 && xAxis>0 && abs(yAxis)>xAxis) { // AREA 8
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis+xAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis==0 && abs(yAxis)>xAxis) { // AREA 9
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis<0 && yAxis<xAxis) { // AREA 10
    Serial1.print("L");
    Serial1.print(yAxis-xAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis<0 && yAxis==xAxis) { // AREA 11
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis<0 && xAxis<0 && yAxis>xAxis) { // AREA 12
    Serial1.print("L");
    Serial1.print(xAxis-yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis==0 && xAxis<0 && yAxis>xAxis) { // AREA 13
    Serial1.print("L");
    Serial1.print(xAxis);
    Serial1.print("R");
    Serial1.print(-xAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis<0 && yAxis<abs(xAxis)) { // AREA 14
    Serial1.print("L");
    Serial1.print(yAxis+xAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis<0 && yAxis==abs(xAxis)) { // AREA 15 
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis>0 && xAxis<0 && yAxis>abs(xAxis)) { // AREA 16
    Serial1.print("L");
    Serial1.print(yAxis+xAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }
  else if(yAxis==0 && xAxis==0) { // AREA 17
    Serial1.print("L");
    Serial1.print(yAxis);
    Serial1.print("R");
    Serial1.print(yAxis);
    Serial1.println("!");
  }

  Esplora.writeRGB(255, 39, 0); // RGB led will be yellow for this motion mode.

  if(switch3==0 && joystickVal==0) {
    int count = 0; // The second cons
    Serial1.println("!MODECHANGE!");
    while(switch3==0 && joystickVal==0) {
      switch3 = Esplora.readButton(SWITCH_UP);
      joystickVal = Esplora.readJoystickSwitch();
      count+=1; //here it counts

      Esplora.writeRGB(0, 0, 0);

      if(count%200==0) {
        Esplora.tone(1200, 50);
        Esplora.writeRGB(255,0,0);
      }

      if(count==800) { //it will count 3 secs
        Esplora.tone(1500, 500); 
        modeInteger+=1;
        Serial1.println("Button mode is on.");
        delay(500);
        break;
      }
      delay(1);
    }
  }
  
  if(switch1==0 && joystickVal==0) {
    int count = 0; // The second cons
    Serial1.println("!PLAYERCHANGE!");
    while(switch1==0 && joystickVal==0) {
      switch1 = Esplora.readButton(SWITCH_DOWN);
      joystickVal = Esplora.readJoystickSwitch();
      count+=1; //here it counts

      Esplora.writeRGB(0, 0, 0);

      if(count%200==0) {
        Esplora.tone(700, 50);
        Esplora.writeRGB(255,0,0);
      }

      if(count==800) { //it will count 3 secs
        Esplora.tone(1500, 500); 
        playerInteger+=1;
        delay(500);
        break;
      }
      delay(1);
    }
  }
}

void start() { // Function for starting animation
  Esplora.writeRGB(255, 255, 255);
  Esplora.tone(4500,100);
  Esplora.writeRGB(0, 0, 0);
  delay(1000);
  Esplora.tone(1000,100);
  Esplora.writeRGB(255, 255, 255);
  delay(75);
  Esplora.tone(2000,100);
  Esplora.writeRGB(0, 0, 0);
  delay(75);
  Esplora.tone(3000,100);
  Esplora.writeRGB(255, 255, 255);
  delay(75);
  Esplora.tone(4000,100);
  Esplora.writeRGB(0, 0, 0);
  delay(75);
  Esplora.tone(5000,100);
  Esplora.writeRGB(255, 255, 255);
  delay(75);
  Esplora.writeRGB(0, 0, 0);
}
