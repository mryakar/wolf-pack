# wolf-pack

In this project two Arduino Robots are controlled wirelessly by Arduino Esplora.

Detailed information is provided in source code as comments. 

`git clone --recursive https://gitlab.com/mryakar/wolf-pack.git`

Project is under GPLv3 licence which means feel free to use and share it.

### *mryakar* ###
